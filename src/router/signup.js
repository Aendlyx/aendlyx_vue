import Vue from 'vue'
import Router from 'vue-router'
import Signup from '@/components/Signup'
import VueResource from 'vue-resource'


Vue.use(Router)
Vue.use(VueResource)

export default new Router({
  routes: [
    {
      path: '/signup',
      name: 'Signup',
      component: Signup
    }
  ]
})
