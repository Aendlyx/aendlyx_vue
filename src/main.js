// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
// import App from './App'
import App from './App2'
import router from './router'
import VueResource from 'vue-resource'
import BootstrapVue from 'bootstrap-vue'
Vue.use(VueResource)
Vue.use(BootstrapVue)
Vue.config.productionTip = false
Vue.http.options.emulateJSON = true
/* eslint-disable no-new */
new Vue({
  http: {root: 'http://192.168.116.24'},
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
